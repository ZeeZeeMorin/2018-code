//package org.usfirst.frc.team4139.robot;
//
//import com.ctre.phoenix.motorcontrol.ControlMode;
//import com.ctre.phoenix.motorcontrol.FeedbackDevice;
//import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
//import com.ctre.phoenix.motorcontrol.can.*;
//
//import edu.wpi.first.wpilibj.IterativeRobot;
//import edu.wpi.first.wpilibj.Joystick;
//import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
//
//public class magneticEncoder extends IterativeRobot {
//
//	/**
//	 * Just a follower, could be a Talon.
//	 */
//	TalonSRX motor1 = new TalonSRX(1);
//	/**
//	 * Master Talon
//	 */
//	TalonSRX motor2 = new TalonSRX(2);
//	/**
//	 * Simple thread to plot the sensor velocity
//	 */
//	PlotThread _plotThread;
//	/**
//	 * joystick or gamepad
//	 */
//	Joystick _joystick = new Joystick(0);
//	
//	public void teleopInit() {
//		/* Victor will follow Talon */
//		motor2.follow(motor1);
//
//		/*
//		 * new frame every 1ms, since this is a test project use up as much
//		 * bandwidth as possible for the purpose of this test.
//		 */
//		motor1.setStatusFramePeriod(StatusFrameEnhanced.Status_2_Feedback0, 1, 10);
//		motor2.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative, 0, 10);
//
//		/* fire the plotter */
//		_plotThread = new PlotThread(this);
//		new Thread(_plotThread).start();
//	}
//
//	public void teleopPeriodic() {
//		/*
//		 * If there is mechanical deflection, eccentricity, or damage in the
//		 * sensor it should be revealed in the plot.
//		 * 
//		 * For example, an optical encoder with a partially damaged ring will
//		 * reveal a periodic dip in the sensed velocity synchronous with each
//		 * rotation.
//		 * 
//		 * This can also be wired to a gamepad to test velocity sweeping.
//		 */
//		if (_joystick.getRawButton(1))
//			motor1.set(ControlMode.PercentOutput, 0.25); /* 25 % output */
//		else 
//			motor2.set(ControlMode.PercentOutput, 0.0);
//	}
//
//	/** quick and dirty threaded plotter */
//	class PlotThread implements Runnable {
//		Robot robot;
//
//		public PlotThread(Robot robot) {
//			this.robot = robot;
//		}
//
//		public void run() {
//			/*
//			 * speed up network tables, this is a test project so eat up all of
//			 * the network possible for the purpose of this test.
//			 */
//			// NetworkTable.setUpdateRate(0.010); /* this suggests each time
//			// unit is 10ms in the plot */
//			while (true) {
//				/* yield for a ms or so - this is not meant to be accurate */
//				try {
//					Thread.sleep(1);
//				} catch (Exception e) {
//				}
//				/* grab the last signal update from our 1ms frame update */
//				double velocity = this.robot.motor1.getSelectedSensorVelocity(0);
//				SmartDashboard.putNumber("vel", velocity);
//			}
//		}
//}
//}
//
//
//
//
////package org.usfirst.frc.team4139.robot;
////
////import java.util.LinkedList;
////
////import org.usfirst.frc.team4139.robot.Grabber;
////import org.usfirst.frc.team4139.robot.Utils.Instruction;
//////import org.usfirst.frc.team4139.robot.Sensors.Controller;
////import org.usfirst.frc.team4139.robot.commands.ExampleCommand;
////import org.usfirst.frc.team4139.robot.subsystems.ExampleSubsystem;
////import org.usfirst.frc.team4139.robot.Arm;
////
////import com.ctre.phoenix.motorcontrol.ControlMode;
//////import edu.wpi.first.wpilibj.Compressor;
////import com.ctre.phoenix.motorcontrol.can.TalonSRX;
////import com.kauailabs.navx.frc.AHRS;
////
//////import edu.wpi.first.wpilibj.Solenoid;
////import edu.wpi.first.wpilibj.*;
////import java.util.LinkedList;
////
////public class magneticEncoder 
////{
////
////	int pulseWidthPos = _tal.getPulseWidthPossition(); //get the decoded pulse width encoder position
////	int pulseWidthUs = _tal.getPulseWidthRiseToFallUs(); //get the pulse width in us, rise-to-fall
////	int periodUs = _tal.getPulseWidthRiseToRiseUs(); //get the period in us, rise-to-rise
////	int pulseWidthVel = _tal.get
////}
