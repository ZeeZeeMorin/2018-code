/*
 * FRC Team 4139 "Easy as Pi"
 * written by Dina Dehaini, Yishai Silver, Dexin Zhou, Tahir Siddiq, and other contributors
 * written for 2018 robotics season - FIRST Power Up
 * 
 * Note:
 * 1. The autonomous code is very messy. I don't know if it works properly or not.
 * 2. We did not end up using the encoder or ultrasonic sensor.
 */

package org.usfirst.frc.team4139.robot;
import java.util.LinkedList;

import org.usfirst.frc.team4139.robot.Utils.Instruction;
import org.usfirst.frc.team4139.robot.commands.ExampleCommand;
import org.usfirst.frc.team4139.robot.subsystems.ExampleSubsystem;
import org.usfirst.frc.team4139.robot.Arm;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.Preferences;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.networktables.NetworkTable;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;


/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.properties file in the
 * project.
 */
public class Robot extends IterativeRobot implements PIDOutput 
{
	/*
	 * port number guide:
	 * 1,4,5,7 wheels
	 * 2,3,6 arm
	 */
	
	private CANClimber climber; 

	private WPI_TalonSRX fLMotor;
	private WPI_TalonSRX rLMotor;
	private WPI_TalonSRX fRMotor;
	private WPI_TalonSRX rRMotor;

	private Arm Arm;

	private LinkedList<Instruction> instructions;

	private Ultrasonic Ultrasonic;

	private boolean autoON = false;

	public static final ExampleSubsystem kExampleSubsystem = new ExampleSubsystem();

	private AHRS navX = new AHRS(SPI.Port.kMXP);

	private Compressor c;
	private Solenoid sol;
	private Solenoid climbsol;

	private Sensors stick;
	private static int AUTO_MODE = 2;
	private CANWheels wheels;
	private Instruction currentInstruction;
	private Timer timer;
	private Timer autoTimer;
	Command m_autonomousCommand;
	SendableChooser<Command> m_chooser = new SendableChooser<>();
	private NetworkTable networktable = NetworkTable.getTable("GRIP/myContoursReport");


	//TRYING GYRO
	PIDController turnController;
	double rotateToAngleRate;

	boolean gripOn;
	boolean turnToTarget;
	double target;
	private ControlMode current;
	private int dist;

	//position of the robot at the start of match
	//1=left, 2=middle, 3=right
	private int robotLocation;
	
	//Preference class is used to get the robot location from SmartDashboard
	Preferences prefs;
	
	/* The following PID Controller coefficients will need to be tuned */
	/* to match the dynamics of your drive system.  Note that the      */
	/* SmartDashboard in Test mode has support for helping you tune    */
	/* controllers by displaying a form where you can enter new P, I,  */
	/* and D constants and test the mechanism.                         */

	static final double kP = 0.03;
	static final double kI = 0.00;
	static final double kD = 0.00;
	static final double kF = 0.00;

	static final double kToleranceDegrees = 2.0f;

	/**
	 * This function is run when the robot is first started up and should be
	 * used for any initialization code.
	 */
	@Override
	public void robotInit() 
	{
		climber = new CANClimber(9);//NUMBER TO BE DETERMINED	
		
		m_chooser.addDefault("Default Auto", new ExampleCommand());
		// chooser.addObject("My Auto", new MyAutoCommand());
		SmartDashboard.putData("Auto mode", m_chooser);

		wheels = new CANWheels(1,4,5,7);
		Arm = new Arm();
		Ultrasonic = new Ultrasonic(0); 

		timer = new Timer();

		c = new Compressor(0);
		sol = new Solenoid(6);
		climbsol = new Solenoid(7);//NUMBER TO BE DETERMINED
		stick = new Sensors();

		c.setClosedLoopControl(true);
		c.setClosedLoopControl(false);

		//TRYING GYRO CODE
		try {
			/***********************************************************************
			 * navX-MXP:
			 * - Communication via RoboRIO MXP (SPI, I2C, TTL UART) and USB.            
			 * - See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface.
			 * 
			 * navX-Micro:
			 * - Communication via I2C (RoboRIO MXP or Onboard) and USB.
			 * - See http://navx-micro.kauailabs.com/guidance/selecting-an-interface.
			 * 
			 * Multiple navX-model devices on a single robot are supported.
			 ************************************************************************/
			navX = new AHRS(SPI.Port.kMXP); 
		} catch (RuntimeException ex ) {
			DriverStation.reportError("Error instantiating navX MXP:  " + ex.getMessage(), true);
		}
		turnController = new PIDController(kP, kI, kD, kF, navX, this);
		turnController.setInputRange(-180.0f,  180.0f);
		turnController.setOutputRange(-1.0, 1.0);
		turnController.setAbsoluteTolerance(kToleranceDegrees);
		turnController.setContinuous(true);

		/* Add the PID Controller to the Test-mode dashboard, allowing manual  */
		/* tuning of the Turn Controller's P, I and D coefficients.            */
		/* Typically, only the P value needs to be modified.                   */
		LiveWindow.addActuator("DriveSystem", "RotateController", turnController);




		fLMotor = new WPI_TalonSRX(1);
		rLMotor = new WPI_TalonSRX(4);
		fRMotor = new WPI_TalonSRX(5);
		rRMotor = new WPI_TalonSRX(7);
		current = fLMotor.getControlMode();
		dist = (int) ((3 / (6 * Math.PI)) * (4 * 1024));
		
		//Get input from Preference in SmartDashboard. 
		//Default robotLocation is 3 (right position)
		prefs=Preferences.getInstance();
		robotLocation=prefs.getInt("location", 1); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<CHOOSING ROBOT LOCATION
	}


	/*
	turnController = new PIDController(kP, kI, kD, kF, navX, this);
    turnController.setInputRange(-180.0f,  180.0f);
    turnController.setOutputRange(-1.0, 1.0);
    turnController.setAbsoluteTolerance(kToleranceDegrees);
    turnController.setContinuous(true);
	 */

	/**-
	 * This function is called once each time the robot enters Disabled mode.
	 * You can use it to reset any subsystem information you want to clear when
	 * the robot is disabled.
	 */
	@Override
	public void disabledInit() {

	}

	@Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}


	@Override
	public void autonomousInit() {
		m_autonomousCommand = m_chooser.getSelected();
		navX.zeroYaw();

		/*
		 * String autoSelected = SmartDashboard.getString("Auto Selector",
		 * "Default"); switch(autoSelected) { case "My Auto": autonomousCommand
		 * = new MyAutoCommand(); break; case "Default Auto": default:
		 * autonomousCommand = new ExampleCommand(); break; }
		 */

		// schedule the autonomous command (example)
		if (m_autonomousCommand != null) {
			m_autonomousCommand.start();
		}
		autoTimer=new Timer();
		autoTimer.start();

		int dist = (int) ((3 / (6 * Math.PI)) * (4 * 1024));

		wheels.switchToTank();

//		rLMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
//		rLMotor.setSelectedSensorPosition(0, 0, 0);
//		fLMotor.set(ControlMode.Follower, 1);
//		
//		fRMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
//		fRMotor.setSelectedSensorPosition(0, 0, 0);
//		rRMotor.set(ControlMode.Follower, 7);

		
		//set grabber to be on at the beginning of the match
		c.start();
		//sol.set(true);
	}

	/**
	 * This function is called periodically during autonomous.
	 */
	@Override
	public void autonomousPeriodic()
	{
		Scheduler.getInstance().run();
		
//		if(autoTimer.get()<1.0)
//		{
//			wheels.drive(.7, .7);
//		}
//		else
//			wheels.drive(0.0, 0.0);



		String gameData;
		gameData = DriverStation.getInstance().getGameSpecificMessage();
		
		//NOTE: please use 0.8 speed for all driving & arm because that's what we used to determine time & distance ratio
		
		// distance(inch) / 77.16 = time
		// robot length = 27 in
		
		//positions:
		//left: align to the left edge
		//middle: align to the right, between station 2 and station 3
		//right: align to the right edge
		
		
		if(gameData.length() > 0)
		{
			if(gameData.charAt(0) == 'R')
			{
				switch(robotLocation)
				{
				case 1://robot on the left side
					//drive forward 140 inches and stop
					System.out.println("R case 1");
					if(autoTimer.get()<4.5)//113 in, that's =140-robot length -------------1.46
					{
						sol.set(false);
						wheels.drive(.5, .5);					
					}
					else
					{
						wheels.drive(0, 0); 
					}
//					if(autoTimer.get()<1)
//					{
//						Arm.up();
//					}
//					else
//					{
//						Arm.stop();					
//					}
					break;

				case 2://robot at the middle
					//drive forward for 113 in, arm up 1 second, drop box at t=2
					System.out.println("R case 2");
					
					sol.set(true);

					if(autoTimer.get()<5.35)//113 in, that's =140-robot length
					{
						wheels.drive(.5, .5);					
					}
					else
					{
						wheels.drive(0, 0);
					}
					if(autoTimer.get()>1.3 && autoTimer.get()<3.5)
					{
						Arm.uP();
					}
					else if(autoTimer.get()>3.5)
					{
						Arm.uP();					
					}
					else
					{
						Arm.stop();//CHJDFKJHJ
					}
					if(autoTimer.get()>5)
					{
						Arm.UP();
						sol.set(false);
					}
					break;

				case 3://robot on the right side
					//was 1.4385(111 in),2.435 arm,5,135,5.6436(39.25 in)
					//drive for 140 in, arm up 1 second, turn left at t=2, drive forward for 25 in, drop box at t=4
					System.out.println("R case 3");
					if(autoTimer.get()<1.0)
					{
						wheels.drive(0, 0);
						System.out.println("delaying");
					}
					if(autoTimer.get()<5.5)//140in.
					{
						wheels.drive(.5, .5);
						System.out.println("straight");
					}
//					if(autoTimer.get()<2)	
//					{
//					//	Arm.up();//arm needs to go up certain amount
//					}
//					else	
//					{
//					//	Arm.stop();
//					}
//					if(autoTimer.get()>3 && autoTimer.get()< 3.7)//90 degrees left
//					{
//						wheels.drive(-0.8, 0.8);
//						System.out.println("turning");
//					}
//					else if(autoTimer.get()>4 && autoTimer.get() < 4.32)//25in
//					{
//						wheels.drive(.8, 8);
//					}
					else if(autoTimer.get()>5)
					{
						sol.set(false);//grabber to open
						wheels.drive(0, 0);
						System.out.println("STOP");
					}
					break;				
				}
			}
			else if(gameData.charAt(0) == 'L')
			{
				switch(robotLocation)
				{
				case 1://robot on the left side
					//drive for 140 in, arm up 1 second, turn right at t=2, drive forward for 25 in, drop box at t=4

					System.out.println("L case 1");
					//sol.set(true);

					if(autoTimer.get()<5.35)//113 in, that's =140-robot length
					{
						wheels.drive(.5, .5);					
					}
					else
					{
						wheels.drive(0, 0);
					}
					if(autoTimer.get()>1.3 && autoTimer.get()<3.5)
					{
					//	Arm.uP();
					}
					else if(autoTimer.get()>3.5)
					{
						//Arm.uP();					
					}
					else
					{
						//Arm.stop();//CHJDFKJHJ
					}
					if(autoTimer.get()>5)
					{
						//Arm.UP();
						//sol.set(false);
					}
					break;

				case 2://robot at the middle
					//drive forward for 113 in, arm up 1 second, don't drop the box
					System.out.println("L case 2");

					if(autoTimer.get()<4.5)//113 in, that's =140-robot length -------------1.46
					{
						sol.set(false);
						wheels.drive(.5, .5);					
					}
					else
					{
						wheels.drive(0, 0); 
					}
//					if(autoTimer.get()<1)
//					{
//						Arm.up();
//					}
//					else
//					{
//						Arm.stop();					
//					}
					break;

				case 3://robot on the right side
					//drive forward 140 inches and stop
					System.out.println("L case 3");

					if(autoTimer.get()<4.5)
					{
						wheels.drive(.5,.5);
					}
					else
					{
						wheels.drive(0, 0);
					}
					break;
				}
			}
		}
	/*
			else if(gameData.charAt(1)=='R')
			{
				if(autoTimer.get()<3.6547)//goes for 282in
				{
					wheels.drive(0.5,0);
					System.out.println("straight");
					Arm.up();// goes up a certain distance

				}

				else if(autoTimer.get()>3.66 && autoTimer.get()< 4.36)//90 degrees right
				{

					wheels.drive(0.8, -0.8);
					System.out.println("turning");

				}
				else if(autoTimer.get()>4.36 && autoTimer.get()<4.57)//goes forward 
				{
					wheels.switchToTank();
					wheels.drive(.5, 0);

				}

				else{//this stops the robot
					sol.set(false);//grabber releases
					wheels.drive(0,0);
					System.out.println("STOP");
				}
			} 
			else if(gameData.charAt(2)=='R')
			{
				autoTimer.reset();
				if(autoTimer.get()<5.9616)//goes for 460in
				{
					wheels.drive(0.8, 0.8);
					System.out.println("straight");
					while(autoTimer.get()<7.5)
					{

						Arm.up();// goes up a certain distance
					}
				}

				else if(autoTimer.get()>5.9616 && autoTimer.get()< 6.66)//90 degrees right
				{

					wheels.drive(0.8, -0.8);
					System.out.println("turning");

				}
				else if(autoTimer.get()>6.66 && autoTimer.get()<6.87)//goes forward 
				{
					wheels.drive(0.8, 0.8);
				}

				else
				{//this stops the robot
					sol.set(false);//grabber releases
					wheels.drive(0,0);
					System.out.println("STOP");
				}
			} 
			*/
		else 
		{
			System.out.println("No string from game data!");
		}		
		
		 
	}

	@Override
	public void teleopInit() { 
		c.start();

		// This makes sure that the autonomous stops running when
		// teleop starts running. If you want the autonomous to
		// continue until interrupted by another command, remove
		// this line or comment it out.
		timer = new Timer();
		timer.reset();
		timer.start();
		//wheels.start();
		wheels.switchToArcade();
		wheels.drive(0, 0);

		stick = new Sensors();
		stick.setArcade();
		//currentInstruction = new NoIntsruction();
		instructions =  new LinkedList<Instruction>();

		navX.zeroYaw();

		turnToTarget = false;
		target = 90;

		//_frontLeftMotor.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);

		autoON = false;

		if (m_autonomousCommand != null) 
		{
			m_autonomousCommand.cancel();
		}

		gripOn = false;
		sol.set(true);
	}

	/**
	 * This function is called periodically during operator control.
	 */
	@Override
	public void teleopPeriodic() 
	{
		//Ultrasonic.printSonicDist();
		Scheduler.getInstance().run();

		//sol.set(true);

		//sol.set(false);
		//if(stick.getButtonA())
		 //{
		 //sol.set(!(sol.get()));
		 //}

		//		if (stick.getButtonX())
		//			wheels.toggleDriveMode();

		wheels.drive(stick.getStickLeft(), stick.getStickRight());

		if (stick.getButtonLBumper()) 
		{
			Arm.up();
		}
		else if (stick.getButtonRBumper())
		{
			Arm.down();
		}
		else
		{
			Arm.stop();
		}

		if (stick.getButtonX())
			sol.set(false);
		else if (stick.getButtonY())
			sol.set(true);
		
		if (stick.getButtonA())
			climbsol.set(false);
		else if (stick.getButtonB())
			climbsol.set(true);

		//System.out.println("Gyro: "+navX.getAngle());


		//gripOn = !gripOn;
//		if(gripOn){
//			System.out.println("auto");
//
//			double[] defaultValue = new double[0];
//
//			double[] xVals = networktable.getNumberArray("centerX", defaultValue);
//			if(xVals.length > 0){
//				if(Math.abs(xVals[0] - 80) < 10)
//					wheels.drive(-0.5, -0.5);
//				else if(xVals[0] < 80) //where 80 is the center x value (of the image)
//					wheels.drive(0.0, -.7); //supposed to turn right
//				else
//					wheels.drive(0.0, .7); //supposed to turn left
//			}
//			else{
//				System.out.println("no table");
//			}
//		}
		
		if(stick.getButtonBackButton()) 
		{
			Arm.uP();
//				timer.reset();
//			if(timer.get()<.5)
//			{
//				Arm.down();
//			}
//		sol.set(true);
		
		}
	}
	//turnToTarget = false;
	//			turnToTarget = !turnToTarget;

	//	if(turnToTarget)
	//	wheels.turnDegree(target);

	//		System.out.println(navX.getAngle() + " --- Angle ---");
	//		System.out.println(navX.getCompassHeading() + "--- Compass ---");
	//		System.out.println(navX.getRoll() + "--- Roll ---");
	//		System.out.println(navX.getYaw() + "--- Yaw ---");
	//		System.out.println(navX.getPitch() + "--- Pitch ---");

	//GYRO
	/*boolean rotateToAngle = true;
		turnController.setSetpoint(90.0f);
		if( stick.getButtonX() ){
			turnController.setSetpoint(-90.0f);
			rotateToAngle = true;
		}
		else if (stick.getButtonY() ){
			turnController.setSetpoint(90.0f);
			rotateToAngle = true;
		}
		double currentRotationRate;
        if ( rotateToAngle ) {
            turnController.enable();
            currentRotationRate = rotateToAngleRate;
        } else {
            turnController.disable();
            //currentRotationRate = stick.getTwist();
        }*/

	//}

	/**
	 * This function is called periodically during test mode.
	 */
	@Override
	public void testPeriodic() 
	{

	}


	@Override
	public void pidWrite(double output) {
		// TODO Auto-generated method stub
		rotateToAngleRate = output;
	}
}
