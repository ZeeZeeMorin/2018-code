package org.usfirst.frc.team4139.robot;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.Timer;

//I don't know who made the up, uP, UP methods.
public class Arm 
{
	private Timer time;
	private TalonSRX Arm1;
	private TalonSRX Arm2;
	private TalonSRX Arm3;

	private int start;

	private boolean Up = false;

	private boolean Down = false;

	public Arm()
	{
		//Arm1.setSelectedSensorPosition(0, 0, 0);

		Arm1 = new TalonSRX(2);
		Arm2 = new TalonSRX(3);
		Arm3 = new TalonSRX(6);

		start = 0;

		//Arm1.setSelectedSensorPosition(0, 0, 0);
	}

	public void up() {

		Up = true;

		Down = false;
		// if(time.get()<1.0){
		Arm1.set(ControlMode.PercentOutput, -1);
		Arm2.set(ControlMode.PercentOutput, -1);
		Arm3.set(ControlMode.PercentOutput, -1);
		//        }
		//        else{
		//        	Arm1.set(ControlMode.PercentOutput,0.0);
		//        	Arm2.set(ControlMode.PercentOutput, 0.0);
		//        }
	}
	public void UP() {

		Up = true;

		Down = false;
		// if(time.get()<1.0){
		Arm1.set(ControlMode.PercentOutput, -.3);
		Arm2.set(ControlMode.PercentOutput, -.4);
		Arm3.set(ControlMode.PercentOutput, -.4);
		//        }
		//        else{
		//        	Arm1.set(ControlMode.PercentOutput,0.0);
		//        	Arm2.set(ControlMode.PercentOutput, 0.0);
		//        }
	}
	
	public void uP() {

		Up = true;

		Down = false;
		// if(time.get()<1.0){
		Arm1.set(ControlMode.PercentOutput, -.7);
		Arm2.set(ControlMode.PercentOutput, -.4);
		Arm3.set(ControlMode.PercentOutput, -.5);
		//        }
		//        else{
		//        	Arm1.set(ControlMode.PercentOutput,0.0);
		//        	Arm2.set(ControlMode.PercentOutput, 0.0);
		//        }
	}

	public void down() {

		Up = false;

		Down = true;
		// if(time.get()<1.0){
		Arm1.set(ControlMode.PercentOutput, .3);
		Arm2.set(ControlMode.PercentOutput, .3);
		Arm3.set(ControlMode.PercentOutput, .3);
		//        }
		//        else{
		//        	Arm1.set(ControlMode.PercentOutput, 0.0);
		//        	Arm2.set(ControlMode.PercentOutput,0.0);
		//        }


	}
	public void stop()
	{
		Arm1.set(ControlMode.PercentOutput, 0);
		Arm2.set(ControlMode.PercentOutput, 0);
		Arm3.set(ControlMode.PercentOutput, 0);
	}

	public void approach(int target){

		if(start == 0)
			Arm1.setSelectedSensorPosition(0, 0, 0);
		else
			start = Arm1.getSelectedSensorPosition(0);

		Arm1.setSelectedSensorPosition(0, 0, 0);
		Arm2.set(ControlMode.Follower, 2);
		Arm3.set(ControlMode.Follower, 2);
		int diff = start - target;

		int direction = -1;
		if (diff > 1)
			direction = 1;
		diff = Math.abs(diff);

		//slow down as you approach the target

		if(diff > 300){

			Arm1.set(ControlMode.PercentOutput, .2 * direction);

		}

		else if (diff > 100){

			Arm1.set(ControlMode.PercentOutput, 0.1 * direction);

		}

		else if (diff > 10)

			Arm1.set(ControlMode.PercentOutput, 0.1 * direction);

		else
			Arm1.set(ControlMode.PercentOutput, 0.0);
	}



//	@Override
//	public void teleopPeriodic() {
//
//		approach(rLMotor.getSelectedSensorPosition(0), 1024*4*4); //THIS IS REALLY THE ONLY LINE THAT MATTERS FOR SETTING POSITION
//
//
//	}

}