package org.usfirst.frc.team4139.robot;

import com.ctre.phoenix.motorcontrol.can.*;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;

import edu.wpi.first.wpilibj.RobotDrive;
//import org.usfirst.frc.team4139.robot.Sensors.Gyroscope;
import org.usfirst.frc.team4139.robot.TurnDir;
//import org.usfirst.frc.team4139.robot.Sensors.Ultrasonic;
//import com.ctre.CANTalon;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.SPI;
/*
 * This class contains methods to invert the motors, drive, and switch between tank and arcade drive 
 * for the teleop portion of the competition. It also contains methods to drive a certain distance and
 * turn to a certain degree for the automated section of the competition.
 */

//import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.Timer;

@SuppressWarnings("deprecation")
public class CANWheels
{
	//NOTE: The ID's for the TalonSRXs must be configured manually on the roboRIO
	private WPI_TalonSRX fLMotor;
	private WPI_TalonSRX rLMotor;
	private WPI_TalonSRX fRMotor;
	private WPI_TalonSRX rRMotor;
	
	private AHRS ahrs = new AHRS(SPI.Port.kMXP);
	
	private RobotDrive wheels;
	
	private int driveMode;
	public static final int TANK_DRIVE = 1;
	public static final int ARCADE_DRIVE = 2;
	
	int idFL, idRL, idFR, idRR;
	
	
	//private Gyroscope gyro;
	private Timer timer;
//	private Ultrasonic sonic; //gota go faast
	
	public static final double circumference = (6*Math.PI);

	public CANWheels(int idFL, int idRL, int idFR, int idRR)
	{		
	//	gyro = new Gyroscope();
	//	gyro.gyroReset();
		
		//sonic = new Ultrasonic(0);
		
		this.idFL = idFL;
		this.idRL = idRL;
		this.idFR = idFR;
		this.idRR = idRR;
				
		fLMotor = new WPI_TalonSRX(idFL);
		rLMotor = new WPI_TalonSRX(idRL);
		fRMotor = new WPI_TalonSRX(idFR);
		rRMotor = new WPI_TalonSRX(idRR);
		
		//wheels = new RobotDrive(FLMotor, RearLeftMotor, FrontRightMotor, RearRightMotor);
		
//		fLMotor = new CANTalon(idFL);
//	    rLMotor = new CANTalon(idRL);
//		fRMotor = new CANTalon(idFR);
//		rRMotor = new CANTalon(idRR);
		
/**		rRMotor.setFeedbackDevice(CANTalon.FeedbackDevice.CtreMagEncoder_Relative);
 *		rRMotor.reverseSensor(true);
 *		rRMotor.configEncoderCodesPerRev(1040);
 *		rRMotor.setPosition(0);
 */		
		wheels = new RobotDrive(fLMotor,rLMotor,fRMotor,rRMotor);
		
		driveMode = TANK_DRIVE;
		
		timer = new Timer();
		timer.reset();
	}
	
	public void start()
	{
	//	gyro.gyroReset();
		timer.reset();
	}
	
	/*public void driveDist(int ft) {
		//distance is in feet
		int dist = (int) ((ft / (6 * Math.PI)) * (4 * 1024));
		
		ControlMode current = fLMotor.getControlMode();
		rLMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
		rLMotor.setSelectedSensorPosition(0, 0, 0);
		fLMotor.set(ControlMode.Follower, idRL);
		
		fRMotor.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, 0, 0);
		fRMotor.setSelectedSensorPosition(0, 0, 0);
		rRMotor.set(ControlMode.Follower, idFR);
		
		for(int i = 0; i < 1000000; i++)
		{			int diff = dist - rLMotor.getSelectedSensorPosition(0);
			if(diff > 4000){
				rLMotor.set(0.5);
				fRMotor.set(0.5);
			}
			else if (diff > 100){
				rLMotor.set(0.1);
				fRMotor.set(0.1);
			}
			else if (diff > 10){
				rLMotor.set(0.06);
				fRMotor.set(0.06);
			}
			else{
				rLMotor.set(0.0);
				fRMotor.set(0.0);
				break;
			}
		}
		fLMotor.set(current, idFL);
		rRMotor.set(current, idFR);
	}*/

	public boolean wait(double seconds)
	{
		if(timer.get() == 0.0)
			timer.start();
		if(timer.get() < seconds)
		{
			//System.out.println("Waiting...");
			return false;
		}
		else
		{
			timer.stop();
			timer.reset();
			return true;
		}
	}	
	
	//This class tells the robot to drive for a certain amount of time (the parameter), at a speed of 0.5
	public boolean driveTime(double mru)
	{ 	
		//robot.switchToArcade();
		if(timer.get() == 0)
		{
			timer.start();
		}
		
		//System.out.println(timer.get());
		
		if(timer.get() < mru)
		{
			wheels.tankDrive(.8,.8);
			//System.out.println("Driving");
			return false;
		}
		else
		{
			wheels.tankDrive(0,0);
			
			timer.stop();
			timer.reset();
			return true;
		}
	}
	
	
//}
	//This class tells the robot to drive for a certain amount of time (the parameter), at a speed of 0.5
	/*public boolean driveTimeSensor(double mru)
	{ 	
		this.switchToArcade();
		if(timer.get() == 0)
		{
			timer.start();
		}
		
		System.out.println(timer.get());
		
		if(timer.get() < mru && sonic.getSonicDist() > 31.0)
		{
			this.drive(-.5,0.0);
			System.out.println("Driving");
			System.out.println("Sonar: "+ sonic.getSonicDist());
			sonic.printSonicDist();
			return false;
		}
		else
		{
			timer.stop();
			timer.reset();
			return true;
		}
	}*/
	public void turn(double degrees, TurnDir turnDir)
    {
        this.switchToArcade();
        ahrs.zeroYaw();
        if(ahrs.getAngle() < degrees)
        {
            //System.out.println("Turning");        
            //System.out.println(ahrs.getYaw());
            switch(turnDir){
            case left:
                this.drive(0,-.8);
                break;
            case right:
                this.drive(0, .8);
                break;
            }    
            //return false;
        }
        else
        {
//            ahrs.reset();
            this.drive(0.0, 0.0);
            //return true;
        }

    }
	//this class tells the robot to turn in a certain direction until it is a certain degree rLom its initial direction.
	/*public boolean turn(double degrees, TurnDir turnDir)
	{
		this.switchToArcade();
		
		if(Math.abs(gyro.getGyroAngle()) < Math.abs(degrees-10))
		{
			System.out.println("Turning");		
			System.out.println(gyro.getGyroAngle());
			
			switch(turnDir){
			case left:
				this.drive(0,-.7);
				break;
			case right:
				this.drive(0, .7);
				break;
			}	
			return false;
		}
		else
		{
			gyro.gyroReset();
			this.drive(0.0, 0.0);
			return true;
		}
	}*/
	
	public boolean turnDegree(double degreeTarget){
		System.out.println(ahrs.getAngle());
		System.out.println(ahrs.getCompassHeading());
		
		if(ahrs.getCompassHeading() < degreeTarget)
			this.drive(0,.7);
		else
			this.drive(0, -.7);
		
		return true;
	}
	

	//switches to TankDrive
	public void switchToTank()
	{
		driveMode = TANK_DRIVE;
	}
	
	//switches to ArcadeDrive
	public void switchToArcade()
	{
		driveMode = ARCADE_DRIVE;
	}
	
	//this method drives the robot, based on the current drive mode. Configured to be easily compatible with the Joystick.
	public void drive(double lS,double rS)
	{
		switch(driveMode)
		{
			case TANK_DRIVE:
				wheels.tankDrive(lS, rS);
				break;
			case ARCADE_DRIVE:
				wheels.arcadeDrive(-lS, -rS);
				break;
			default:
				wheels.tankDrive(lS, rS);
		}
	}
	
	public void toggleDriveMode()
	{
		if(driveMode == TANK_DRIVE)
			this.switchToArcade();
		else
			this.switchToTank();
	}
		
}