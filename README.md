### Source code for the 2018 Robotics Season for the game **"FIRST Power UP"**

**FRC Team 4139 "Easy as Pi"**

Written by Dina Dehaini, Yishai Silver, Dexin Zhou, Tahir Siddiq, Luke Kim, and other contributors

January to March 2018

## Files:
- Robot.java -- main file
- Arm.java
- CANWheels.java
- Sensors.java --methods for controller inputs (I don't know why that's there)

## Imported libraries:
- WPILIB
- CTRE Phoenix (for Talon SRX)
- navX